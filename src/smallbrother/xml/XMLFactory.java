package smallbrother.xml;

import smallbrother.Meeting;
import smallbrother.account.Account;
import smallbrother.account.Admin;
import smallbrother.account.User;


public class XMLFactory
{
    public static String toXML(Account account)
    {
        if(account instanceof User)
        {
            String s="<Employee type=\"user\">\n<Name>"+XMLUtil.stringToXml(account.getName())+ "</Name>\n";
            s+="<Password>"+XMLUtil.stringToXml(account.getPassword())+"</Password>\n";
            s+="<Sick>"+XMLUtil.stringToXml(String.valueOf(((User) account).isSick()))+"</Sick>\n";
            s+="<IsInsideBuilding>"+XMLUtil.stringToXml(String.valueOf(((User) account).isInsideBuilding()))+"</IsInsideBuilding>\n";
            if(!(((User) account).getMeetings().isEmpty()))
            {
                for(Meeting m:((User) account).getMeetings())
                    s+=m.toXML();
            }
            s+="</Employee>\n";
            return s;
        }
        if(account instanceof Admin)
        {
            String s="<Employee type=\"admin\">\n<Name>"+XMLUtil.stringToXml(account.getName())+ "</Name>\n";
            s+="<Password>"+XMLUtil.stringToXml(account.getPassword())+"</Password>\n";
            s+="</Employee>\n";
            return s;
        }
        return null;
    }
}
