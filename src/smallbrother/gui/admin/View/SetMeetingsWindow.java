package smallbrother.gui.admin.View;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import smallbrother.gui.admin.Controller.AdminController;

/**
 * @author  dv6
 */
public class SetMeetingsWindow extends JFrame
{
	private JPanel contentPane;
	private JPanel meetingMainPanel = new JPanel();
	private DefaultListModel amodel = new DefaultListModel();
	private DefaultListModel pmodel = new DefaultListModel();
	private JLabel labelAllEmps = new JLabel("All employees");
	private JLabel labelParticipants = new JLabel("Participants");
	private JLabel lblDate = new JLabel("Date");
	private JLabel lblStart = new JLabel("Start ");
	private JLabel lblEnd = new JLabel("End");
	private JLabel startTimeLabel = new JLabel("Example: 12:00");
	private JLabel endTimeLabel = new JLabel("Example: 12:30");
	private JLabel lblExample = new JLabel("Example: 01/01/2011");
	private JList listOfAllEmps = new JList(amodel);
	private JList listOfParticipants = new JList(pmodel);
	private JButton btnAdd = new JButton("Add");
	private JButton btnRemove = new JButton("Remove");
	private JButton btnSetMeeting = new JButton("Set Meeting");
	private JButton btnCancel = new JButton("Cancel");
	private JTextField dateField = new JTextField();
	private JTextField startTimeField = new JTextField();
	private JTextField endTimeField = new JTextField();
	private ArrayList<String> participants = new ArrayList<String>();
	private JSeparator separator = new JSeparator();
	private String startTime = "";
	private String date = "";
	private String endTime = "";
	/**
	 * @uml.property  name="controller"
	 * @uml.associationEnd  
	 */
	private AdminController controller;

	public SetMeetingsWindow(AdminController controller)
	{
		this.controller = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 455, 381);
		setTitle("Schedule Meetings");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		contentPane.add(meetingMainPanel, BorderLayout.CENTER);
		meetingMainPanel.setLayout(null);
		
		// Positions 
		listOfAllEmps.setBounds(10, 30, 153, 171);
		listOfParticipants.setBounds(268, 30, 153, 171);
		labelAllEmps.setBounds(10, 11, 83, 14);
		labelParticipants.setBounds(332, 11, 83, 14);
		btnAdd.setBounds(173, 44, 89, 23);
		btnRemove.setBounds(173, 78, 89, 23);
		lblDate.setBounds(10, 232, 60, 14);
		dateField.setBounds(52, 231, 86, 20);
		btnSetMeeting.setBounds(330, 228, 89, 23);
		btnCancel.setBounds(332, 295, 89, 23);
		lblExample.setBounds(143, 231, 143, 14);
		separator.setBounds(20, 212, 399, 2);
		startTimeField.setBounds(52, 265, 86, 20);
		lblStart.setBounds(10, 265, 46, 14);
		endTimeField.setBounds(52, 296, 86, 20);
		lblEnd.setBounds(10, 299, 46, 14);
		startTimeLabel.setBounds(143, 268, 143, 14);
		endTimeLabel.setBounds(143, 299, 87, 14);
		
		// adding to panel
		meetingMainPanel.add(listOfAllEmps);
		meetingMainPanel.add(listOfParticipants);
		meetingMainPanel.add(labelAllEmps);
		labelParticipants.setHorizontalAlignment(SwingConstants.RIGHT);
		meetingMainPanel.add(labelParticipants);
		meetingMainPanel.add(btnAdd);
		meetingMainPanel.add(btnRemove);
		meetingMainPanel.add(btnCancel);
		meetingMainPanel.add(btnSetMeeting);
		meetingMainPanel.add(lblDate);
		meetingMainPanel.add(dateField);
		dateField.setColumns(10);
		lblExample.setFont(new Font("Tahoma", Font.ITALIC, 9));
		meetingMainPanel.add(lblExample);
		meetingMainPanel.add(separator);
		meetingMainPanel.add(startTimeField);
		startTimeField.setColumns(10);
		meetingMainPanel.add(lblStart);
		meetingMainPanel.add(endTimeField);
		endTimeField.setColumns(10);
		meetingMainPanel.add(lblEnd);
		startTimeLabel.setFont(new Font("Tahoma", Font.ITALIC, 9));
		meetingMainPanel.add(startTimeLabel);
		endTimeLabel.setFont(new Font("Tahoma", Font.ITALIC, 9));
		meetingMainPanel.add(endTimeLabel);
		
		btnAdd.addActionListener(new AddToParticipantsHandler());
		btnRemove.addActionListener(new RemoveFromParticipantsHandler());
		btnCancel.addActionListener(new CancelSetMeetingHandler());
		btnSetMeeting.addActionListener(new SetMeetingHandler());
	}
	
	public void displayAllEmps(ArrayList<String> inEmps)
	{
		String[] normalArray = null;
		normalArray = new String[inEmps.size()];  
	    normalArray = inEmps.toArray(normalArray);
		for(int i = 0; i < normalArray.length; i++)
		{
			amodel.addElement(normalArray[i]);
		}
	}
	
	public void addToPAL(String participant)
	{
		if(!(participants.contains(participant) && !(pmodel.isEmpty())))
		{
			participants.add(participant);
			pmodel.addElement(listOfAllEmps.getSelectedValue());
		}
		else
		{
			JOptionPane.showMessageDialog(new JFrame(), "User is already a participant", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		System.out.println(participants.toString());
	}
	
	public void removeFromPAL(String participant)
	{
		while(participants.contains(participant) && !(pmodel.isEmpty()))
		{
		participants.remove(participants.indexOf(participant));
		pmodel.remove(listOfParticipants.getSelectedIndex());
		}
		System.out.println(participants.toString());
	}
	
	
	public void toController(String date, String startTime, String endTime, ArrayList<String> aL)
	{
		controller.setMeeting(date, startTime, endTime, aL);
	}
	
	public void showError(String text)
	{
		JOptionPane.showMessageDialog(new JFrame(), text, "Dialog",
				JOptionPane.ERROR_MESSAGE);
	}
	
	private class AddToParticipantsHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			String name = listOfAllEmps.getSelectedValue().toString();
			addToPAL(name);
		}
	}
	private class RemoveFromParticipantsHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			String name = listOfParticipants.getSelectedValue().toString();
			removeFromPAL(name);
		}
	}
	private class CancelSetMeetingHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			pmodel.clear();
			participants.clear();
			dispose();
		}
	}
	private class SetMeetingHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			date = dateField.getText();
			startTime = startTimeField.getText();
			endTime = endTimeField.getText();
			if(!(date.isEmpty() || startTime.isEmpty() || endTime.isEmpty()))
			{
				toController(date, startTime, endTime, participants);
				dispose();				
			}
			else
			{
				showError("Date or time invalid");
			}
		}
	}
}