package smallbrother.account;

import java.io.Serializable;

/**
 * @author  dv6
 */
public abstract class Account implements Serializable
{
	/**
	 * @author   dv6
	 */
	protected enum AccountType
	{
		/**
		 * @uml.property  name="aDMIN"
		 * @uml.associationEnd  
		 */
		ADMIN, /**
		 * @uml.property  name="uSER"
		 * @uml.associationEnd  
		 */
		USER
	};

	/**
	 * @uml.property  name="accountType"
	 * @uml.associationEnd  
	 */
	private AccountType accountType;
	/**
	 * @uml.property  name="name"
	 */
	private String name;
	/**
	 * @uml.property  name="password"
	 */
	private String password;

	public Account(AccountType accountType, String name, String password)
	{
		this.accountType = accountType;
		this.name = name;
		this.password = password;
	}

	/**
	 * @return
	 * @uml.property  name="name"
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 * @uml.property  name="name"
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return
	 * @uml.property  name="password"
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 * @uml.property  name="password"
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return
	 * @uml.property  name="accountType"
	 */
	public AccountType getAccountType()
	{
		return accountType;
	}

	public abstract String toString();
}
