package smallbrother.xml;


public class XMLUtil
{
	public static String stringToXml( String s )
	{
		String res = "";
        if(s!=null)
		for( int i = 0; i < s.length(); ++i )
			switch( s.charAt(i) )
			{
				case '&': res += "&amp;"; break;
				case '<': res += "&lt;"; break;
				case '>': res += "&gt;"; break;

				default: res += s.charAt(i);
			}

		return res;
	}
}