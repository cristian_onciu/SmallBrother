package smallbrother.rmi;

import com.sun.script.javascript.RhinoScriptEngine;
import smallbrother.Facade;
import smallbrother.gui.admin.Controller.AdminController;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

/**
 * @author  dv6
 */
public class Server implements RMI_ServerInterface,Serializable {

    /**
	 * @uml.property  name="facade"
	 * @uml.associationEnd  
	 */
    protected Facade facade;
    private Map<RMI_ClientInterface,String > users=new HashMap<RMI_ClientInterface ,String>();
    /**
	 * @uml.property  name="ac"
	 * @uml.associationEnd  
	 */
    private AdminController ac;


    public Server(AdminController ac) {
        super();
        this.ac=ac;

       try
        {
            this.facade=new Facade();
            System.out.println("Constructing server...");
            RMI_ServerInterface server= (RMI_ServerInterface) UnicastRemoteObject.exportObject(this, 1099);
            System.out.println("Binding server to registry");
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("smallbrother", server);
            System.out.println("Server binded on " + InetAddress.getLocalHost()+"\nWaiting for invocations from clients...");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }


    /**
	 * @return
	 * @uml.property  name="facade"
	 */
    public Facade getFacade()
    {
        return facade;
    }


    public String sendXML(String userName) throws RemoteException
    {
    	System.out.println("Printing from server.java");
        System.out.println(facade.getUser(userName));
        return facade.getUser(userName);
    }


    @Override
    public void login(RMI_ClientInterface clientInterface, String userName, String password) throws RemoteException
    {
        if(facade.login(userName,password))
        {
            users.put(clientInterface,userName);
            ac.updateUsersList();
        }
    }

    @Override
    public void logout(String userName) throws RemoteException
    {
        facade.logout(userName);
        users.remove(userName);
        ac.updateUsersList();
    }
    
    @Override
    public boolean isInsideBuilding(String userName)
    {
    	return facade.isInsideBuilding(userName);
    }
    
    @Override
    public boolean isSick(String userName)
    {
    	return facade.isSick(userName);
    }
    
    @Override
    public void isHealthy(String userName)
    {
    	facade.registerHealthy(userName);
    }
    
    @Override
    public String getMeetingsForUser(String userName)
    {
    	return facade.getMeetingsForUser(userName);
    }
    
    @Override
    public String getAllParticipantsForMeeting(int meetingID)
    {
        return facade.getAllParticipantsForMeeting(meetingID);
    }
}
