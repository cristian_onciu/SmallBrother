package smallbrother.gui.user.View;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import smallbrother.gui.user.Controller.UserController;

/**
 * @author  dv6
 */
public class RegularUserView extends JFrame
{
	private JPanel contentPane;
	private JPanel centerPanel = new JPanel();
	private JTextArea meetingTextarea = new JTextArea();
	private JLabel labelStatus = new JLabel("Not logged in");
	private String userName = "";
	private String passWord = "";
	/**
	 * @uml.property  name="ipAddress"
	 */
	private String ipAddress = "";
	private JButton logOutButton = new JButton("Log out");
	private JButton loginButton = new JButton("Log in");
	private JButton btnRefresh = new JButton("Refresh");
	/**
	 * @uml.property  name="controller"
	 * @uml.associationEnd  
	 */
	private UserController controller;
	private final JPanel topPanel = new JPanel();
	private final JPanel southPanel = new JPanel();
	private final JScrollPane scrollPane = new JScrollPane();

	public RegularUserView(UserController controller)
	{
		this.controller = controller;
		setResizable(false);
		setEnabled(true);
		setVisible(true);
		setTitle("Small Brother");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 440, 330);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(null);
		scrollPane.setBounds(0, 0, 424, 226);

		centerPanel.add(scrollPane);
		scrollPane.setViewportView(meetingTextarea);
		meetingTextarea.setText("You have to log in to see the meetings");
		setContentPane(contentPane);

		contentPane.add(topPanel, BorderLayout.NORTH);
		topPanel.add(btnRefresh);
		topPanel.add(labelStatus);
		labelStatus.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(southPanel, BorderLayout.SOUTH);
		southPanel.add(loginButton);
		southPanel.add(logOutButton);
		logOutButton.addActionListener(new LogoutHandler());
		loginButton.addActionListener(new LoginHandler());
		btnRefresh.addActionListener(new RefreshHandler());
		setStartState();

	}

	public JTextArea textAreaSettings()
	{
		meetingTextarea.setFont(new Font("Arial", Font.PLAIN, 10));
		meetingTextarea.setEditable(false);
		meetingTextarea.setBackground(Color.WHITE);
		meetingTextarea.setForeground(Color.BLACK);
		meetingTextarea.setText("List of meetings");
		return meetingTextarea;
	}

	public void displayMeetings()
	{
		meetingTextarea.setText("");
//		System.out.println("displaymeetings method in GUI");
//		System.out.println("Printing out in GUI: "
//				+ controller.getMyMeetings(userName));
		meetingTextarea.append(controller.getMeetings(userName));
//		meetingTextarea.append(controller.getParticipantsForMeetingID(userName).toString());
	}

	public void displayNothing()
	{
		meetingTextarea.setText("Not connected");
	}

	public void showError(String text)
	{
		JOptionPane.showMessageDialog(new JFrame(), text, "Dialog",
				JOptionPane.ERROR_MESSAGE);
	}

	public String getName()
	{
		return userName;
	}

	public void setStartState()
	{
		btnRefresh.setEnabled(false);
		logOutButton.setEnabled(false);
		loginButton.setEnabled(true);
		labelStatus.setText("Not logged in");
		displayNothing();
	}

	public void setLiState()
	{
		btnRefresh.setEnabled(true);
		logOutButton.setEnabled(true);
		loginButton.setEnabled(false);
	}

	public String getPassword()
	{
		return passWord;
	}

	/**
	 * @return
	 * @uml.property  name="ipAddress"
	 */
	public String getIpAddress()
	{
		return ipAddress;
	}
	
	public void setLabel()
	{
		boolean isHere = controller.isInsideBuilding(userName);
		if(isHere == true)
		{
			labelStatus.setText("Logged in as: " + userName);
		}
		else
		{
			setStartState();
		}
	}
	
	

	private class LoginHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			userName = JOptionPane.showInputDialog(null, "Enter your name");
			if (userName == null)
			{
				showError("Nothing entered");
			}
			else
			{
				System.out.println("Username: " + userName);
				passWord = JOptionPane.showInputDialog(null,
						"Enter your password");
			}
			if (passWord == null)
			{
				showError("Nothing entered");
			}
			else
			{
				System.out.println("Password: " + passWord);
				ipAddress = JOptionPane.showInputDialog(null,
						"Enter IP address");
			}
			if (ipAddress == null)
			{
				showError("Nothing entered");
			}
			else
			{
				if (!(userName == null || passWord == null || ipAddress == null || ipAddress
						.equals("")))
				{
					System.out.println("IpAddress: " + ipAddress);
					controller.login(ipAddress, userName, passWord);
					setLiState();
				}
				else
				{
					showError("Could not connect");
				}
			}

		}
	}

	private class LogoutHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			controller.logout(userName);
			controller.exitProgram();
		}
	}

	private class RefreshHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			displayMeetings();
			setLabel();
			controller.setHealthy(userName);
		}
	}


}
