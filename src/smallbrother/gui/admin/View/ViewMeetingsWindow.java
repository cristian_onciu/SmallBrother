package smallbrother.gui.admin.View;

import smallbrother.gui.admin.Controller.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @author  dv6
 */
public class ViewMeetingsWindow extends JFrame
{

	private JPanel contentPane;
	private JPanel mainPanel = new JPanel();
	private JTextArea txtrDisplayAllMeetings = new JTextArea();
	/**
	 * @uml.property  name="controller"
	 * @uml.associationEnd  
	 */
	private AdminController controller;

	public ViewMeetingsWindow(AdminController controller)
	{
        this.controller=controller;
		setTitle("View meetings");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1200, 300);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		contentPane.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(null);
		
		txtrDisplayAllMeetings.setText("");
		txtrDisplayAllMeetings.setBounds(10, 11, 1150, 240);
        txtrDisplayAllMeetings.setLineWrap(true);

        JScrollPane sb;
        sb=new JScrollPane(txtrDisplayAllMeetings);
        sb.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        txtrDisplayAllMeetings.setAutoscrolls(true);
        txtrDisplayAllMeetings.append(displayMeetings());
		mainPanel.add(txtrDisplayAllMeetings);
	}
	
	public String displayMeetings()
	{
		
		System.out.println(controller.viewAllMeetings().get(1));
		return controller.viewAllMeetings().toString();
	}
}
