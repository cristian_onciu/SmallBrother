package smallbrother;

import java.util.ArrayList;

/**
 * @author  dv6
 */
public class MeetingList
{

	/**
	 * @uml.property  name="meeting"
	 * @uml.associationEnd  
	 */
	private Meeting meeting;
	/**
	 * @uml.property  name="participantUsers"
	 */
	private ArrayList<String> participantUsers = new ArrayList<String>();

	/**
	 * @return
	 * @uml.property  name="meeting"
	 */
	public Meeting getMeeting()
	{
		return meeting;
	}

	/**
	 * @param meeting
	 * @uml.property  name="meeting"
	 */
	public void setMeeting(Meeting meeting)
	{
		this.meeting = meeting;
	}

	/**
	 * @return
	 * @uml.property  name="participantUsers"
	 */
	public ArrayList<String> getParticipantUsers()
	{
		return participantUsers;
	}

	public void addParticipant(String userName)
	{
		participantUsers.add(userName);
	}

	@Override
	public String toString()
	{
		return " " + meeting + ", participantUsers=" + participantUsers + "\n";
	}
}
