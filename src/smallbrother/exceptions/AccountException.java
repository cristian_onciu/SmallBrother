package smallbrother.exceptions;

public class AccountException extends Exception
{
	String userName;

	public AccountException(String userName)
	{
		super("Account " + userName + " exception.");
		this.userName = userName;
	}

	public String userExists()
	{
		return this.getMessage() + userName + " exists!";
	}

	public String userDoesNotExist()
	{
		return this.getMessage() + userName + " doesn't exist!";
	}
}
