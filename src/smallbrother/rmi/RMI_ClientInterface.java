package smallbrother.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMI_ClientInterface extends Remote
{
	public String getXML(String userName) throws RemoteException;
}
