package smallbrother.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import smallbrother.Meeting;
import smallbrother.account.Account;
import smallbrother.account.Admin;
import smallbrother.account.User;

import javax.swing.text.DateFormatter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLPersonnelParser
{
    private static Map<String,User> users=new HashMap<String,User>();
    private static Document dom;

    public static Map parseXmlFile(File file)
       {
		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			dom = db.parse(file);
            parseDocument();
        }catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
        return users;
	}

    private static void parseDocument()
    {
		//get the root element
		Element root = dom.getDocumentElement();

		//get a nodelist of elements
		NodeList nl = root.getElementsByTagName("Employee");
        //NodeList nlMeeting = dom.getDocumentElement().getChildNodes();
		if(nl != null && nl.getLength() > 0) {
			for(int i = 0 ; i < nl.getLength();i++) {

				//get the employee element
				Element element = (Element)nl.item(i);


				//get the User
				User user = (User) getUser(element);
                System.out.println(user.toString());

				//map it
				users.put(user.getName(),user);
			}
		}
	}

    private static Account getUser(Element empElement)
    {
       //for each <Employee> element get text or int values of
	    //name ,password, sick, inside
	    String type = empElement.getAttribute("type");
        String name = getTextValue(empElement,"Name");
        String password = getTextValue(empElement,"Password");
        Boolean sick= Boolean.parseBoolean(getTextValue(empElement,"Sick"));
        Boolean insideBuilding= Boolean.parseBoolean(getTextValue(empElement,"IsInsideBuilding"));

        //Create a new User with the value read from the xml nodes
        if(type.equals("user"))
        {
            User user=new User(name,password);
            user.setSick(sick);
            user.setInsideBuilding(insideBuilding);
             //if(empElement.getTagName().equals("Meeting"))
            for(int i=0;i<empElement.getChildNodes().getLength();i++)
            {
                //System.out.println(empElement.getChildNodes().item(i).getNodeName());
                if(empElement.getChildNodes().item(i).getNodeName().trim().equals("Meeting"))
                user.scheduleMeeting(getMeeting((Element) empElement.getChildNodes().item(i)));
                //user.scheduleMeeting(getMeeting(empElement));
            }
            return user;
        }

        if(type.equals("admin"))
            return new Admin(name,password);
        return null;
    }

    private static String getTextValue(Element element, String tagName)
    {
        String textVal = null;
        NodeList nl = element.getElementsByTagName(tagName);
        if(nl != null && nl.getLength() > 0) {
            Element el = (Element)nl.item(0);
            textVal = el.getFirstChild().getNodeValue().trim();
        }

        return textVal;
    }

    private static int getIntValue(Element ele, String tagName)
    {
		return Integer.parseInt(getTextValue(ele,tagName));
	}

    private static Meeting getMeeting(Element element)
    {
        try {
            int id= getIntValue(element,"Id");
            System.out.println(id);
            Date begin= DateFormat.getDateTimeInstance().parse(getTextValue(element, "Begin"));
            Date end= DateFormat.getDateTimeInstance().parse(getTextValue(element, "End"));
            Meeting m=new Meeting(begin,end);
            m.setId(id);
            return m;
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }
}
