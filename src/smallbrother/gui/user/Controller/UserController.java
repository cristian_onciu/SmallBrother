package smallbrother.gui.user.Controller;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import smallbrother.gui.user.View.RegularUserView;
import smallbrother.rmi.RMI_ClientInterface;
import smallbrother.rmi.RMI_ServerInterface;


/**
 * @author  dv6
 */
public class UserController extends UnicastRemoteObject implements
		RMI_ClientInterface
{

	/**
	 * @uml.property  name="view"
	 * @uml.associationEnd  
	 */
	private RegularUserView view; 
	/**
	 * @uml.property  name="rmiServer"
	 * @uml.associationEnd  
	 */
	private RMI_ServerInterface rmiServer = null;

	protected UserController() throws RemoteException
	{
		super();
		System.out.println("Connection established");
	}

	@Override
	public String getXML(String userName) throws RemoteException
	{
		return rmiServer.sendXML(view.getName());
	}

	public void exitProgram()
	{
		System.exit(0);
	}

	public void login(String serverIP, String userName, String password)
	{
		try
		{
			Registry registry = LocateRegistry.getRegistry(serverIP, 1099);
			rmiServer = (RMI_ServerInterface) registry.lookup("smallbrother");
			rmiServer.login(this, userName, password);
			System.out.println("Connection established\n "
					+ rmiServer.sendXML(userName));
			getMeetings(userName);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		catch (NotBoundException e)
		{
			e.printStackTrace();
		}
	}

	public String getMeetings(String userName)
	{
		String temp = "";
		try
		{
			temp = rmiServer.getMeetingsForUser(userName);
		}
		catch (RemoteException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out
				.println("printing out in getMeetings method in UserController");
		System.out.println(temp);
		return temp;

	}

	public void logout(String userName)
	{
		try
		{
			if (rmiServer != null)
				rmiServer.logout(userName);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean isInsideBuilding(String userName)
	{
		boolean isHere = false;
		try
		{
			isHere = rmiServer.isInsideBuilding(userName);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		return isHere;
	}

	public boolean isSick(String userName)
	{
		boolean isSick = false;
		try
		{
			isSick = rmiServer.isSick(userName);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		return isSick;
	}
	
	public void setHealthy(String userName)
	{
		try
		{
			rmiServer.isHealthy(userName);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
	}
	public ArrayList<String> getParticipantsForMeetingID(String userName)
	{
		ArrayList<String> participants=new ArrayList<String>();
		for(int i=0;i<this.getMyMeetings(userName).size();i++)
		{
			try
			{
				participants.add(rmiServer.getAllParticipantsForMeeting(i));
			}
			catch (RemoteException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return participants;
	}
	public ArrayList<Integer> getMyMeetings(String userName)
	{
		System.out.println("This is getMyMeetings in the controller:");
		ArrayList<Integer> meetingsID=new ArrayList<Integer>();
		String meetings = this.getMeetings(userName);
		char m = ' '; 
		String l="";
		ArrayList<String> line = new ArrayList<String>();
		for(int i = 0; i < meetings.length(); i++)
		{
			m=meetings.charAt(i);
			//System.out.println(m);
			if(m!='\n')l+=m;
			else{
				line.add(l);
				l="";
			}
		}
		for(String a:line)
			if(a.startsWith("Meeting ID:"))meetingsID.add(Integer.valueOf(a.charAt(11)));
			//meetingsID = meetings.charAt(12);
		return meetingsID;
	}
}
