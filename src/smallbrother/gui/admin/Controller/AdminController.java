package smallbrother.gui.admin.Controller;

import smallbrother.Facade;
import smallbrother.MeetingList;
import smallbrother.gui.admin.View.AdminView;
import smallbrother.gui.admin.View.SetMeetingsWindow;
import smallbrother.rmi.Server;
import java.util.ArrayList;


/**
 * @author  dv6
 */
public class AdminController
{

	/**
	 * @uml.property  name="view"
	 * @uml.associationEnd  
	 */
	AdminView view; 
	/**
	 * @uml.property  name="smw"
	 * @uml.associationEnd  
	 */
	SetMeetingsWindow smw;
	/**
	 * @uml.property  name="server"
	 * @uml.associationEnd  
	 */
	Server server;
	/**
	 * @uml.property  name="f"
	 * @uml.associationEnd  
	 */
	Facade f;

	public AdminController()
	{
		this.server = new Server(this);
		this.f = server.getFacade();
	}

	public ArrayList<String> getAllEmployees()
	{
		return f.getAllEmployees();
	}

	public ArrayList<String> getSickEmployees()
	{
		return f.getSickEmployees();
	}

	public ArrayList<String> getLoggedInEmps()
	{
		return f.getLoggedInEmps();
	}

	public ArrayList<String> getLoggedOutEmps()
	{
		return f.getLoggedOutEmps();
	}

	public ArrayList<MeetingList> viewAllMeetings()
	{
		return f.getAllMeetings();
	}

	public void setMeeting(String date, String startTime, String endTime,
			ArrayList<String> meetingList)
	{
		String[] dateA;
		String[] startTimeA;
		String[] endTimeA;
		dateA = date.split("/");
		startTimeA = startTime.split(":");
		endTimeA = endTime.split(":");

		int day = Integer.parseInt(dateA[0]);
		int month = Integer.parseInt(dateA[1]);
		int year = Integer.parseInt(dateA[2]);
		int hour_begin = Integer.parseInt(startTimeA[0]);
		int minute_begin = Integer.parseInt(startTimeA[1]);
		int hour_end = Integer.parseInt(endTimeA[0]);
		int minute_end = Integer.parseInt(endTimeA[1]);

		f.scheduleMeeting(meetingList, day, month, year, hour_begin,
				minute_begin, hour_end, minute_end);
		System.out.println(meetingList.toString());
		System.out.println("Day: " + day + ", Month: " + month + ", Year: "
				+ year);
		System.out.println("Hour begins: " + hour_begin + ", Minute begin: "
				+ minute_begin);
		System.out.println("Hour ends: " + hour_end + ", minute ends: "
				+ minute_end);
	}

	public void registerSick(String employeeName)
	{
		f.registerSick(employeeName);
		System.out.println("Registered sick: " + employeeName);
	}

	public void registerHealthy(String employeeName)
	{
		f.registerHealthy(employeeName);
		System.out.println("Registered healthy: " + employeeName);
	}

	public void signUserIn(String employeeName)
	{
		f.login(employeeName);
		System.out.println("Logging in: " + employeeName);
	}

	public void signUserOut(String employeeName)
	{
		f.logout(employeeName);
		System.out.println("Logging out: " + employeeName);
	}

	public void createEmp(String userName, String passw)
	{
		f.createUser(userName, passw);
		System.out.println("Created user: " + userName + ", with password: "
				+ passw);
	}

	public void removeEmp(String userName)
	{
		f.removeUser(userName);
		System.out.println("Removed user: " + userName);
	}

	public void exitProgram()
	{
		System.exit(0);
	}

	public void saveFile()
	{
		f.toFile();
	}

	public void openFile()
	{
		f.getAllDataFromXMLFile();
	}

	public void updateUsersList()
	{
		f = server.getFacade();
	}
}
